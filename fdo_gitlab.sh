#!/bin/bash

export GITLAB_TOKEN="glpat-mzHxxGfPgEZozHTHyFuq" #cli management
export PROJECT_ID="18254"                        # Sudbury
export GITLAB_INSTANCE="https://gitlab.freedesktop.org"

# Function to get all items from a paginated GitLab API endpoint
get_all_items() {
	local endpoint=$1
	local array_name=$2
	local page=1
	while true; do
		response=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_INSTANCE/$endpoint?page=$page&per_page=100" 2>/dev/null)
		current_items=$(echo "$response" | jq -r 'if length > 0 then .[] | .id else empty end')
		if [ -z "$current_items" ]; then
			break
		fi
		eval "$array_name+=( $current_items )"
		page=$((page + 1))
	done
}

function delete_artifacts() {
	./delete-fdo-artifacts.py
}

reverseArray() {
	if shopt -q extdebug; then
		_ArrayToReverse=("${BASH_ARGV[@]}")
	else
		local -n _ArrayToReverse=$1
		shopt -s extdebug
		"${FUNCNAME}" "${_ArrayToReverse[@]}"
		shopt -u extdebug
	fi
}

function delete_pipelines() {
	pipeline_array=()
	get_all_items "api/v4/projects/$PROJECT_ID/pipelines" pipeline_array
	reverseArray pipeline_array
	# Count how many pipelines we found out
	NUMOFPIPES=${#pipeline_array[@]}

	# Print statistics
	echo -e "---\nID:$PROJECT_ID has $NUMOFPIPES pipelines to delete."

	# Print all pipeline IDs from the array
	for pipelineid in "${pipeline_array[@]}"; do
		echo "Deleting pipeline ID: ${pipelineid} -> $GITLAB_INSTANCE/api/v4/projects/$PROJECT_ID/pipelines/${pipelineid}"
		curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --request "DELETE" "$GITLAB_INSTANCE/api/v4/projects/$PROJECT_ID/pipelines/$pipelineid"
	done
}

d_artifacts=
d_pipelines=
while getopts ":ap" o; do
	case "${o}" in
	a) d_artifacts=1 ;;
	p) d_pipelines=1 ;;
	*) exit ;;
	esac
done
shift $((OPTIND - 1))

if [ -n "$d_artifacts" ]; then
	delete_artifacts
fi
if [ -n "$d_pipelines" ]; then
	delete_pipelines
fi
